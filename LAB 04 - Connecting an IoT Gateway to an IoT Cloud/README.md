**LAB 04 - Connecting an IoT Gateway to an IoT Cloud**   
This lab explains how to connect an IoT gateway (Raspberry Pi with Node-Red) to an IoT cloud platform (IBM Watson IoT Cloud Platform with Node-Red). IoT Gateway collects data from its sensors and sends them to the IoT Cloud. IoT Cloud platform then sends commands back to the IoT gateway where it displays a message accordingly.
