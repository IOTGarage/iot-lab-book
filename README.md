# IOT Lab Book

This repository holds supplementary material required to complete the lab tutorials presented in IOT LAB BOOK.




- **LAB 01 - Micro-Controller Programming**  
This lab explains how to program a microcontroller (Arduino) and a few sensors and actuators connected to it. It also explains how to hand events in microcontrollers.


- **LAB 02 - Single-board Computer Programming**  
This lab explains how to program a single-board (Raspberry Pi) to read data sensors and actuate based on the sensor readings. It also explains how to use a display.

- **LAB 03 - Posting Data to an IoT Cloud Platform**  
This lab explains how to configure Thingsboard in a way that we can send data from Rasberry Pi via MQTT node that we install to Node-RED. We will observe a live data on Thingsboard and create customized widgets on the Thingsboard dashboard. 

- **LAB 04 - Connecting an IoT Gateway to an IoT Cloud**   
This lab explains how to connect an IoT gateway (Raspberry Pi with Node-Red) to an IoT cloud platform (IBM Watson IoT Cloud Platform with Node-Red). IoT Gateway collects data from its sensors and sends them to the IoT Cloud. IoT Cloud platform then sends commands back to the IoT gateway where it displays a message accordingly.

- **LAB 05 - Connecting a Sensor Node to IoT Gateway**  
This lab explains how to connect edge development board (Arduino - Sensor Node) to Raspberry Pi (IoT Gateway) over Bluetooth Serial. We will learn how to use Bluetooth command line utilities while utilizing AT command set to configure the Bluetooth adaptor.

- **LAB 06 - End to End Full Stack IoT Development**  
This lab explains how to develop and end-to-end IoT stack that contains edge, fog, and cloud nodes. You will combine what you have learned during the previous labs. The data from edge to fog will be sent over Bluetooth Serial. Then, Raspberry Pi will send the data to Thingsboard cloud where you will observe via dashboard.

- **LAB 07 - Introduction to Wireshark on Raspberry Pi**  
This lab explains how to use the most famous networ packet analyzers. We will install Wireshark on Raspberry Pi. Then we will learn how to observe the wireless network traffic. We will check the main packet features such as source/destination IP then filter the upcoming network packets. We also will learn how to save the captured traffic.

- **LAB 08 - Programming Arduino with Blockly**  
This lab explains how to program Arduino via Blockly. We will control LED bar via rotary angle sensor while using drag and drop nodes which are built-in in Blockly.

- **LAB 09 - Programming Raspberry Pi with Python**  
This lab explains how use grove sensors via Python scripts. Python comes pre-installed with Raspberry Pi OS. We will connect Grove sensors to Raspberry Pi via GrovePi+. Then we will run Python scripts and program several sensors including ultrasonic ranger.

- **LAB 10 - Bluetooth Low Energy (BLE) Based Systems**  
This lab explains how to establish a Bluetooth Low Energy (BLE) connection between the edge development board and mobile phone via application. We will utilize AT commands again to communicate. 

- **LAB 11 - RFID and NFC Based Tracking**  
This lab explains how to utilize NFC and RFID sensors. We will learn how to connect both RFID and NFC modules via UART port. We will turn on the LED in case of there is a successfull communication. 

- **LAB 12 - Multimedia Communication**  
This lab explains how to utilize thumb joystick and speaker. We will see how thumb joystick generates coordinate as we see in game controllers. We will control the colours of LCD via joystick.

- **LAB 13 - Microcontroller Programming Simulator**  
This lab explains how to use a simulation program to simulate a scenario where we control LED, and LCD. This will be useful when we do not have any access actual devices.

- **LAB 14 - Advance Sensors, Actuators, Components**  
This lab explains to utilize Electromyography Sensor (EMG) Sensor via Arduino. We will generate signals via moving our arm muscles. 

- **LAB 15 - 3D Objects Designing and Printing**  
This lab explains how to design 3D objects online. We will design a tool box which contains different sections. Then, we will generate and .STL file which can be inputted to 3D printers to print the actual design.

- **LAB 16 - Getting Started with Raspberry Pi Camera**  
This lab explains how to connect a Raspberry Pi camera to Raspberry Pi to take images and record video. We will also learn how to apply some basic image filters.



## Additional Resources

[Download SD Card Images](https://cf-my.sharepoint.com/:u:/g/personal/pererac_cardiff_ac_uk/EdoTVqOQWstCuuc9_NeWwrABd3rGHgHAcARpvUYw7VpvKA)
