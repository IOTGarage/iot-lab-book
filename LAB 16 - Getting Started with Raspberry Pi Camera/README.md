**LAB 16 - Getting Started with Raspberry Pi Camera**   
This lab explains how to connect a Raspberry Pi camera to Raspberry Pi to take images and record video. We will also learn how to apply some basic image filters.
