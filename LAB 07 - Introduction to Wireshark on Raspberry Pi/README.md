**LAB 07 - Introduction to Wireshark on Raspberry Pi**  
This lab explains how to use the most famous networ packet analyzers. We will install Wireshark on Raspberry Pi. Then we will learn how to observe the wireless network traffic. We will check the main packet features such as source/destination IP then filter the upcoming network packets. We also will learn how to save the captured traffic.
