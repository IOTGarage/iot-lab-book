**LAB 05 - Connecting a Sensor Node to IoT Gateway**   
This lab explains how to connect edge development board (Arduino - Sensor Node) to Raspberry Pi (IoT Gateway) over Bluetooth Serial. We will learn how to use Bluetooth command line utilities while utilizing AT command set to configure the Bluetooth adaptor.


1) To run an Arduino program (.ino files), you need have that file in a folder with the same name.

2) Please download the whole directory (folder) and then run and upload the programs.


