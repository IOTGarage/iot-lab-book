**LAB 02 - Single-board Computer Programming**   
This lab explains how to program a single-board (Raspberry Pi) to read data sensors and actuate based on the sensor readings. It also explains how to use a display.
