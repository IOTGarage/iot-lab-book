**LAB 03 - Posting Data to an IoT Cloud Platform**   
This lab explains how to configure Thingsboard in a way that we can send data from Rasberry Pi via MQTT node that we install to Node-RED. We will observe a live data on Thingsboard and create customized widgets on the Thingsboard dashboard.

- No code files required for Lab 03
